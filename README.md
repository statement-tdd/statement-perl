# statement-perl

Perl starter project for TDD workshop.


## Run tests

```
prove t/statement.t
```

## Run coverage

```
perl -MDevel::Cover t/statement.t
```


## Docker

### Build image
```
docker build -t statement-perl .
```

### run interactive shell
```
docker run -v "$(pwd):/usr/src/app" -it statement-perl /bin/bash
```


