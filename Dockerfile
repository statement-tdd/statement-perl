FROM perl:5.34.0

WORKDIR /usr/src/app

RUN groupadd --gid 1000 perl && useradd --uid 1000 --gid perl --shell /bin/bash --create-home perl
RUN set -ex && apt-get update && apt-get install -y --no-install-recommends software-properties-common dirmngr
RUN cpanm Test::Spec File::Slurp JSON::Parse Test::Exception
RUN cpanm Devel::Cover
RUN cpanm Number::Format
RUN rm -r /root/.cpanm

CMD ["/bin/bash", "-c"]